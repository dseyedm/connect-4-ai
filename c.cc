#define MAX_D 9
//#define CDEBUG
#include <bits/stdc++.h>
#include <stdint.h>
using namespace std;
const char* results[] = { "Draw", "Player victory!", "AI victory!", "Tie!" };
const char* symbols[4][3] = { 
	{
		"   ",
		"   ",
		"   "
	},
	{
		"   ",
		"XXX",
		"   ",
	},
	{
		" O ",
		" O ",
		" O "
	},
	{
		"???",
		"???",
		"???"
	}
};
const char symbols_small[] = { ' ', 'X', 'O', '?' };
enum { BLANK = 0, P1, AI, FULL };
#define X 7
#define Y 6
struct board_t {
	uint8_t b[Y][X];
	bool operator < (const board_t& other) const { return memcmp(b, other.b, sizeof(b)) < 0; }
};
void print_board(const board_t& b) {
	printf(" ");
	for(int x = 0; x < X; ++x) {
		printf("  %d   ", x);
	}
	printf("\n");
	for(int y = 0; y < Y; ++y) {
		for(int i = 0; i < 3; ++i) {
			printf("| ");
			for(int x = 0; x < X; ++x) {
				printf("%s | ", symbols[b.b[y][x]][i]);
			}
			printf("\n");
		}
		printf("-");
		for(int x = 0; x < X; ++x) printf(" - - -");
		printf("\n");
	}
}
void print_board_small(int d, const board_t& b) {
	for(int y = 0; y < Y; ++y) {
		for(int z = 0; z < d; ++z) printf(">");
		printf("|");
		for(int x = 0; x < X; ++x) {
			printf("%c|", symbols_small[b.b[y][x]]);
		}
		printf("\n");
	}
}
int win(const board_t& b, int& heuristic_ai) {
	heuristic_ai = 0;
	// vertical
	for(int x = 0; x < X; ++x) {
		for(int y = 0; y + 3 < Y; ++y) {
			int8_t A = b.b[y+0][x];
			int8_t B = b.b[y+1][x];
			int8_t C = b.b[y+2][x];
			int8_t D = b.b[y+3][x];
			bool AB = A == B, BC = B == C, CD = C == D;
			if(A != BLANK && AB && BC && CD) return A;
			if(A == BLANK && B == AI)            heuristic_ai += BC && CD;
			if(B == BLANK && A == AI && C == AI) heuristic_ai += CD;
			if(C == BLANK && A == AI && D == AI) heuristic_ai += AB;
			if(D == BLANK && A == AI)            heuristic_ai += AB && BC;
			if(A == BLANK && B == P1)            heuristic_ai -= BC && CD;
			if(B == BLANK && A == P1 && C == P1) heuristic_ai -= CD;
			if(C == BLANK && A == P1 && D == P1) heuristic_ai -= AB;
			if(D == BLANK && A == P1)            heuristic_ai -= AB && BC;
		}
	}
	// horizontal
	for(int y = 0; y < Y; ++y) {
		for(int x = 0; x + 3 < X; ++x) {
			int8_t A = b.b[y][x+0];
			int8_t B = b.b[y][x+1];
			int8_t C = b.b[y][x+2];
			int8_t D = b.b[y][x+3];
			bool AB = A == B, BC = B == C, CD = C == D;
			if(A != BLANK && AB && BC && CD) return A;
			if(A == BLANK && B == AI)            heuristic_ai += BC && CD;
			if(B == BLANK && A == AI && C == AI) heuristic_ai += CD;
			if(C == BLANK && A == AI && D == AI) heuristic_ai += AB;
			if(D == BLANK && A == AI)            heuristic_ai += AB && BC;
			if(A == BLANK && B == P1)            heuristic_ai -= BC && CD;
			if(B == BLANK && A == P1 && C == P1) heuristic_ai -= CD;
			if(C == BLANK && A == P1 && D == P1) heuristic_ai -= AB;
			if(D == BLANK && A == P1)            heuristic_ai -= AB && BC;
		}
	}
	// slope up
	for(int y = 3; y < Y; ++y) {
		for(int x = 0; x + 3 < X; ++x) {
			int8_t A = b.b[y-0][x+0];
			int8_t B = b.b[y-1][x+1];
			int8_t C = b.b[y-2][x+2];
			int8_t D = b.b[y-3][x+3];
			bool AB = A == B, BC = B == C, CD = C == D;
			if(A != BLANK && AB && BC && CD) return A;
			if(A == BLANK && B == AI)            heuristic_ai += BC && CD;
			if(B == BLANK && A == AI && C == AI) heuristic_ai += CD;
			if(C == BLANK && A == AI && D == AI) heuristic_ai += AB;
			if(D == BLANK && A == AI)            heuristic_ai += AB && BC;
			if(A == BLANK && B == P1)            heuristic_ai -= BC && CD;
			if(B == BLANK && A == P1 && C == P1) heuristic_ai -= CD;
			if(C == BLANK && A == P1 && D == P1) heuristic_ai -= AB;
			if(D == BLANK && A == P1)            heuristic_ai -= AB && BC;
		}
	}
	// slope down
	for(int y = 0; y + 3 < Y; ++y) {
		for(int x = 0; x + 3 < X; ++x) {
			int8_t A = b.b[y+0][x+0];
			int8_t B = b.b[y+1][x+1];
			int8_t C = b.b[y+2][x+2];
			int8_t D = b.b[y+3][x+3];
			bool AB = A == B, BC = B == C, CD = C == D;
			if(A != BLANK && AB && BC && CD) return A;
			if(A == BLANK && B == AI)            heuristic_ai += BC && CD;
			if(B == BLANK && A == AI && C == AI) heuristic_ai += CD;
			if(C == BLANK && A == AI && D == AI) heuristic_ai += AB;
			if(D == BLANK && A == AI)            heuristic_ai += AB && BC;
			if(A == BLANK && B == P1)            heuristic_ai -= BC && CD;
			if(B == BLANK && A == P1 && C == P1) heuristic_ai -= CD;
			if(C == BLANK && A == P1 && D == P1) heuristic_ai -= AB;
			if(D == BLANK && A == P1)            heuristic_ai -= AB && BC;
		}
	}
	// full? 
	int x;
	for(x = 0; x < X; ++x) {
		if(b.b[0][x] == BLANK) break;
	}
	if(x >= X) return FULL;
	return BLANK;
}
// returns 100 if the AI won.
// returns -100 if the PLAYER won.
// returns [0, 7] for the best move.
// returns -1 if the game is over.
pair<int, int> minimax(int d, board_t& b, bool ai_to_play, int alpha, int beta) {
	int heuristic_ai;
	int winner = win(b, heuristic_ai); 
	if(d >= MAX_D || winner != BLANK) {
		if(winner == AI) return make_pair(INT_MAX, -1);
		else if(winner == P1) return make_pair(INT_MIN, -1);
		return make_pair(heuristic_ai, -1);
	}

	int best_x = 0;
	for(int x = 0; x < X; ++x) {
		if(b.b[0][x] != P1 && b.b[0][x] != AI) {
			int y; 
			for(y = Y - 1; y >= 0; --y) { 
				if(b.b[y][x] != P1 && b.b[y][x] != AI) {
					b.b[y][x] = ai_to_play ? AI : P1;
					break; 
				}
			}
			pair<int, int> score_ai = minimax(d + 1, b, !ai_to_play, alpha, beta);
			b.b[y][x] = BLANK;

			if(ai_to_play) {
				if(score_ai.first > alpha) { 
					alpha = score_ai.first; 
					best_x = x;
				}
			} else {
				if(score_ai.first < beta) { 
					beta = score_ai.first;
					best_x = x; 
				}
			}
			if(alpha >= beta) break;
		}
	}
#ifdef CDEBUG
	{
		int y; 
		for(y = Y - 1; y >= 0; --y) { 
			if(b.b[y][best_x] != P1 && b.b[y][best_x] != AI) {
				b.b[y][best_x] = ai_to_play ? AI : P1;
				break; 
			}
		}
		print_board_small(d, b);
		b.b[y][best_x] = BLANK;
		for(int i = 0; i < d; ++i) printf(">"); 
		printf("^^^best move for %s is %d, best score for %s is %d\n", ai_to_play ? "AI" : "PL", best_x, ai_to_play ? "AI" : "PL", best);
	}
#endif
	return make_pair(ai_to_play ? alpha : beta, best_x);
}
int think(const board_t& b) {
	board_t cpy = b;
	// if no one has moved yet
	{
		int n_checkers = 0;
		for(int i = 0; i < Y; ++i) {
			for(int j = 0; j < X; ++j) {
				if(b.b[i][j] == AI || b.b[i][j] == P1) ++n_checkers;
			}
		}
		if(n_checkers <= 0) return 3;
	}
	pair<int, int> p = minimax(0, cpy, true, INT_MIN, INT_MAX);
	printf("best score is %d and best move is %d\n", p.first, p.second);
	return p.second;
}
int main() {
	srand(time(NULL));
	board_t b;
	for(int y = 0; y < Y; ++y) {
		for(int x = 0; x < X; ++x) {
			b.b[y][x] = BLANK;
		}
	}
	int turn;
	{
		char choice;
		do {
			printf("(a)i or (h)uman to move?\n");
			scanf("%c", &choice);
		} while(choice != 'a' && choice != 'h');
		if(choice == 'a') turn = AI;
		else turn = P1;
	}
	int result = BLANK;
	while(result == BLANK) {
#ifndef CDEBUG
//		system("clear");
#endif
		print_board(b);

		int move = -1;
		if(turn == AI) {
			printf("thinking... "); fflush(stdout);
			move = think(b);
		} else {
			do {
				printf("move: ");
				scanf("%d", (int*)&move);
			} while((move < 0) || (move >= X) || (b.b[0][move] == AI) || (b.b[0][move] == P1));
		}
		assert(move >= 0);
		assert(move < X);
		assert(b.b[0][move] == BLANK);

		{
			int y;
			for(y = Y - 1; y >= 0; --y) {
				if(b.b[y][move] != P1 && b.b[y][move] != AI) {
					b.b[y][move] = turn;
					break;
				}
			}
			assert(y >= 0);
		}

		if(turn == P1) turn = AI;
		else turn = P1;

		int junk;
		result = win(b, junk);
		printf("chance of winning: %d\n", junk);
	}
#ifndef CDEBUG
//	system("clear");
#endif
	print_board(b);
	printf("%s\n", results[result]);
	return 0;
}
